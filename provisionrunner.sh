#!/usr/bin/env bash

#gitlab-runner register  --url https://gitlab.com  --token glrt-rySysMu52tzWeBMWxAkP
echo "Running provision"

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash

apt-get install -y gitlab-runner

source /home/vagrant/.env
rm /home/vagrant/.env

gitlab-runner register --non-interactive --url $URL_GITLAB_RUNNER --token $TOKEN_GITLAB_RUNNER --executor $EXECUTOR_GITLAB_RUNNER --name $NAME_GITLAB_RUNNER

gitlab-runner start
echo "Adding gitlab to docker group"
usermod -aG docker gitlab-runner
newgrp docker

